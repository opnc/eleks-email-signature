import React from 'react';

export default class ResultTable extends React.Component {
  render() {
    return (
      <table
        id={this.props.id}
        cellPadding="0"
        cellSpacing="0"
        width="360"
        style={{background: '#fff'}}
      >
        <tbody>
          <tr>
            <td colSpan="2">
              <p style={{
                font: '15px/1 Arial',
                margin: '0 0 5px'
              }}>Best regards,</p>
            </td>
          </tr>
          <tr>
            <td style={{
              borderRight: '1px solid #f2f2f2',
              padding: '32px 20px 5px 0',
              verticalAlign: 'top',
              width: '140px'
            }}>
              <img
                src="https://media.eleks.com/media/newsletter/eleks-templates/logo.png"
                alt=""
                style={{verticalAlign: 'top'}}
              />
              {this.props.selectedCity.value && (<div>
                <p style={{
                  margin: '20px 0 -10px',
                  font: '14px/20px Arial'
                }}>{this.props.selectedCity.address}</p>
              </div>)}
              <p style={{margin: '18px 0 5px'}}>
                <a
                  href="https://eleks.com"
                  target="_blank"
                  style={{
                    color: '#0045cf',
                    font: '14px/1 Arial',
                    textDecoration: 'none'
                  }}
                >eleks.com</a>
              </p>
            </td>
            <td style={{
              padding: '36px 0 5px 20px',
              verticalAlign: 'top'
            }}>
              {this.props.fullName && (<p style={{
                color: '#0045cf',
                font: '15px/1 Arial',
                margin: '0 0 5px',
                textTransform: 'uppercase',
                wordBreak: 'break-all'
              }}>
                <strong style={{fontWeight: 'bold'}}>{this.props.fullName}</strong>
              </p>)}
              {this.props.position && (<p style={{
                color: '#000',
                font: '15px/20px Arial',
                margin: '0 0 14px',
                wordBreak: 'break-all'
              }}>{this.props.position}</p>)}
              {this.props.mobilePhone && (<table
                cellPadding="0"
                cellSpacing="0"
                width="100%"
              >
                <tbody>
                  <tr>
                    <td style={{
                      padding: '2px 10px 1px 0',
                      verticalAlign: 'top',
                      width: '16px'
                    }}>
                      <img
                        src="https://media.eleks.com/media/newsletter/eleks-templates/phone-icon.png"
                        alt=""
                        height="16"
                        width="16"
                      />
                    </td>
                    <td style={{
                      color: '#000',
                      font: '14px/16px Arial',
                      padding: '1px 0 3px',
                      verticalAlign: 'middle',
                      wordBreak: 'break-all'
                    }}>
                      {this.props.mobilePhone}
                    </td>
                  </tr>
                </tbody>
              </table>)}
              {this.props.landlinePhone && (<table
                cellPadding="0"
                cellSpacing="0"
                width="100%"
              >
                <tbody>
                  <tr>
                    <td style={{
                      padding: '2px 10px 1px 0',
                      verticalAlign: 'top',
                      width: '16px'
                    }}>
                      <img
                        src="https://media.eleks.com/media/newsletter/eleks-templates/landline-phone-icon.png"
                        alt=""
                        height="16"
                        width="16"
                      />
                    </td>
                    <td style={{
                      color: '#000',
                      font: '14px/16px Arial',
                      padding: '1px 0 3px',
                      verticalAlign: 'middle',
                      wordBreak: 'break-all'
                    }}>
                      {this.props.landlinePhone}
                    </td>
                  </tr>
                </tbody>
              </table>)}
              {this.props.skype && (<table
                cellPadding="0"
                cellSpacing="0"
                width="100%"
              >
                <tbody>
                  <tr>
                    <td style={{
                      padding: '2px 10px 1px 0',
                      verticalAlign: 'top',
                      width: '16px'
                    }}>
                      <img
                        src="https://media.eleks.com/media/newsletter/eleks-templates/skype-icon.png"
                        alt=""
                        height="16"
                        width="16"
                      />
                    </td>
                    <td style={{
                      color: '#000',
                      font: '14px/16px Arial',
                      padding: '1px 0 3px',
                      verticalAlign: 'middle',
                      wordBreak: 'break-all'
                    }}>
                      {this.props.skype}
                    </td>
                  </tr>
                </tbody>
              </table>)}
            </td>
          </tr>
        </tbody>
      </table>
    )
  }
}