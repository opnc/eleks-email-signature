import React from 'react';

export default class FormElement extends React.Component {
  render() {
    return (
      <div className="form__element">
        <input
          className={`form__control ${this.props.inputValue ? 'form__control--filled' : ''}`}
          id={this.props.inputName}
          name={this.props.inputName}
          onChange={this.props.inputOnChange}
          type="text"
          value={this.props.inputValue}
        />
        <label
          className="form__label"
          htmlFor={this.props.inputName}
        >{this.props.labelText}{this.props.inputRequired && <sup className="form__label__required">*</sup>}</label>
        {this.props.inputIcon && (<span className="form__icon" dangerouslySetInnerHTML={{__html: this.props.inputIcon}}></span>)}
      </div>
    )
  }
}